import SwadeActiveEffect from './documents/active-effect/SwadeActiveEffect';
import { isFirstGM, isFirstOwner } from './util';

export default class SwadeSocketHandler {
  identifier = 'system.swade';

  constructor() {
    this.registerSocketListeners();
  }

  /** registers all the socket listeners */
  registerSocketListeners(): void {
    game.socket?.on(this.identifier, (data) => {
      switch (data.type) {
        case 'deleteConvictionMessage':
          this._onDeleteConvictionMessage(data);
          break;
        case 'newRound':
          this._onNewRound(data);
          break;
        case 'removeStatusEffect':
          this._onRemoveStatusEffect(data);
          break;
        case 'giveBennies':
          this._onGiveBenny(data);
          break;
        default:
          this._onUnknownSocket(data.type);
          break;
      }
    });
  }

  emit<T extends EventData>(data: T) {
    return game.socket?.emit(this.identifier, data);
  }

  deleteConvictionMessage(messageId: string) {
    this.emit<DeleteConvictionMessageEvent>({
      type: 'deleteConvictionMessage',
      messageId,
      userId: game.userId!,
    });
  }

  removeStatusEffect(uuid: string) {
    this.emit<RemoveStatusEffectEvent>({
      type: 'removeStatusEffect',
      effectUUID: uuid,
    });
  }

  newRound(combatId: string) {
    this.emit<NewRoundEvent>({
      type: 'newRound',
      combatId: combatId,
    });
  }

  giveBenny(users: string[]) {
    this.emit<GiveBenniesEvent>({ type: 'giveBennies', users });
  }

  protected async _onRemoveStatusEffect(data: RemoveStatusEffectEvent) {
    const effect = (await fromUuid(data.effectUUID)) as SwadeActiveEffect;
    if (isFirstOwner(effect.parent)) {
      effect.expire();
    }
  }

  protected _onDeleteConvictionMessage(data: DeleteConvictionMessageEvent) {
    const message = game.messages?.get(data.messageId);
    //only delete the message if the user is a GM and the event emitter is one of the recipients
    if (game.user!.isGM && message?.whisper.includes(data.userId)) {
      message?.delete();
    }
  }

  //advance round
  protected async _onNewRound(data: NewRoundEvent) {
    if (isFirstGM()) {
      game.combats?.get(data.combatId, { strict: true }).nextRound();
    }
  }

  protected _onUnknownSocket(type: string) {
    console.warn(`The socket event ${type} is not supported`);
  }

  protected async _onGiveBenny(data: GiveBenniesEvent) {
    if (data.users.includes(game.userId!)) {
      await game.user?.getBenny();
    }
  }
}

interface EventData {
  type: string;
}

interface RemoveStatusEffectEvent extends EventData {
  effectUUID: string;
}

interface DeleteConvictionMessageEvent extends EventData {
  userId: string;
  messageId: string;
}

interface NewRoundEvent extends EventData {
  combatId: string;
}

interface GiveBenniesEvent extends EventData {
  users: string[];
}
